<?php

use Illuminate\Http\Request;

Route::resource('tasks', 'TaskController',[
    'except' => ['create', 'edit', 'show']
]);
